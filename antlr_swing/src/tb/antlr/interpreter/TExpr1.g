tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (druk //drukowanie na ekran
        | e=expr
        | dek )* //obsługa deklaracji zmiennej
        ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr)    {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr)    {$out = $e1.out - $e2.out;} //dopisanie działania odejmowania
        | ^(MUL   e1=expr e2=expr)    {$out = $e1.out * $e2.out;} //dopisanie działania mnożenia
        | ^(DIV   e1=expr e2=expr)    {$out = div($e1.out, $e2.out);} //dopisanie działania dzielenia z obsługą błędu
        | ^(PODST i1=ID   e2=expr)    {$out = setVariable($i1.text, $e2.out);} //działanie podstawienia 
        | ^(BITAND   e1=expr e2=expr) {$out = bitSum($e1.out, $e2.out);} // suma bitów
        | ^(BITOR    e1=expr e2=expr) {$out = bitOr($e1.out, $e2.out);} // alternatywa bitów
        | ^(COMPARE  e1=expr e2=expr) {$out = compare($e1.out, $e2.out);} // porównanie
        | ID                          {$out = getValue($ID.text);} //pobranie wartości zmiennej
        | INT                         {$out = getInt($INT.text);}
        ;
        catch [RuntimeException e] {System.out.println(e.getMessage());}

//deklaracja zmiennej       
dek 
    : ^(VAR i1=ID) {createVariable($i1.text);}       
    ;

druk
    : ^(PRINT e=expr) {drukuj ($e.text + " = " + $e.out.toString());}
    ;
        
