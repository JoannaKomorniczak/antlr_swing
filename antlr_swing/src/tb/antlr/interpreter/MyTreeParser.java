package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {
	
	private GlobalSymbols globalSymbols = new GlobalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	//operacja dzielenia z obsługą wyjątku
	protected Integer div(Integer a, Integer b) throws RuntimeException{
		if (b==0) {
			throw new RuntimeException("Dzielenie przez zero wywołało wyjątek.");
		}
		return a/b;
	}
	
	//operacja podstawienia
	protected Integer setVariable(String name, Integer value) { 
		globalSymbols.setSymbol(name, value);
		return value;    
	}
	
	//pobranie wartości dla zmiennej
	protected Integer getValue(String name) {
		return globalSymbols.getSymbol(name);
	}
	
	//tworzenie zmiennej
	protected void createVariable(String name) {
		globalSymbols.newSymbol(name);
	}
	
	//suma bitów
	protected Integer bitSum(Integer a, Integer b){
		return a&b;
	}
	
	//alternatywa bitów
	protected Integer bitOr(Integer a, Integer b){
		return a|b;
	}
	
	//porównanie (zwracajace int)
	protected Integer compare(Integer a, Integer b){
		if(a==b) {
			return 1;
		}
		return 0;
	}
}
